/////////////////////////////////////////////////////////////////////////////
//
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file cat.h
/// @version 1.0
///
/// @author Bodie Collins <bodie@hawaii.edu>
/// @date 14 Feb 2022
/////////////////////////////////////////////////////////////////////////////
#pragma once

const double CATPOWER_IN_A_JOULE       = 0 ;
const char CATPOWER      = 'c';


extern double fromCatPowerToJoule( double catPower ) ;
extern double fromJouleToCatPower( double joule ) ;

